from django.contrib import admin

# Register your models here.
from .models import User, Classroom, Action

admin.site.register(User)
admin.site.register(Classroom)
admin.site.register(Action)
