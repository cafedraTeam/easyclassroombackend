import base64
import io
import json
import re

import numpy as np
from PIL import Image
from django.core import serializers
from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
from pyzbar.pyzbar import decode

from main import barcode
from .constants import RequestType, Role
from .models import User, Classroom, Action


def get_key(user_id, classroom_number, timestamp):
    user = User.objects.get(pk=user_id)
    classroom = Classroom.objects.get(pk=classroom_number)
    if classroom.current_user is not None:
        return HttpResponse(status=409, content="This classroom already in use")
    if len(user.classrooms.all()) and user.role == Role.STUDENT.value:
        return HttpResponse(status=409, content="Hey, you already have a key!")
    classroom.current_user = user
    Action.objects.create(
        classroom=classroom,
        user=user,
        timestamp=timestamp,
        type=RequestType.GET_KEY.value
    )
    classroom.save()
    return HttpResponse(status=200, content="You got your key successfully")


def put_key(user_id, classroom_number, timestamp):
    user = User.objects.get(pk=user_id)
    classroom = Classroom.objects.get(pk=classroom_number)
    if classroom.current_user is None:
        return HttpResponse(status=409, content="This classroom already free")
    if classroom not in user.classrooms.all():
        return HttpResponse(status=409, content="Hey, you don't have this key!")
    classroom.current_user = None
    Action.objects.create(
        classroom=classroom,
        user=user,
        timestamp=timestamp,
        type=RequestType.PUT_KEY.value
    )
    classroom.save()
    return HttpResponse(status=200, content="You put your key successfully")


def get_actions():
    actions = Action.objects.all().values()

    for i in range(len(actions)):
        actions[i]['user'] = str(User.objects.get(pk=actions[i]['user_id']))
        actions[i]['timestamp'] = str(actions[i]['timestamp'].timestamp())

    return JsonResponse(list(actions), safe=False)


def get_all_classrooms():
    return JsonResponse(list(Classroom.objects.all().values()), safe=False)


def recognize(src):
    img = barcode.readb64(src)
    res = barcode.barcode_detection(img)
    if not res:
        return HttpResponse("not recognized")
    else:
        try:
            user_model = User.objects.get(stud_number=res[0])
            alternative = json.dumps(model_to_dict(user_model))
            return JsonResponse(serializers.serialize('json', [user_model]), safe=False)
        except User.DoesNotExist:
            return HttpResponse("user doest not exist")

