from django.urls import path

from . import views

urlpatterns = [
    path('get', views.get_key),
    path('put', views.put_key),
    path('actions/getAll', views.get_actions),
    path('classrooms/getAll', views.get_all_classrooms),
    path('recognize', views.recognize),
]
