from enum import Enum


class RequestType(Enum):
    GET_KEY = 'GET_KEY'
    PUT_KEY = 'PUT_KEY'


class Role(Enum):
    STUDENT = 'STUDENT'
    TEACHER = 'TEACHER'
    CAFEDRA = 'CAFEDRA'