import json
from datetime import datetime

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from . import service


def index(request):
    return render(request, "index.html")


# rest urls
@csrf_exempt
def get_key(request):
    query_dict = json.loads(request.body)
    timestamp = datetime.fromtimestamp(int((query_dict['timestamp']) / 1000))
    print(timestamp)
    return service.get_key(
        query_dict['user_id'],
        query_dict['classroom_number'],
        timestamp
    )


@csrf_exempt
def put_key(request):
    query_dict = json.loads(request.body)
    timestamp = datetime.fromtimestamp(int((query_dict['timestamp']) / 1000))
    print(timestamp)
    return service.put_key(
        query_dict['user_id'],
        query_dict['classroom_number'],
        timestamp
    )


@csrf_exempt
def get_actions(request):
    return service.get_actions()


@csrf_exempt
def get_all_classrooms(request):
    return service.get_all_classrooms()


@csrf_exempt
def recognize(request):
    query_dict = json.loads(request.body)
    return service.recognize(query_dict['img'])
