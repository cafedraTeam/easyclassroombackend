from django.urls import path, include, re_path

from . import views

urlpatterns = [
    re_path(r'^api/', include('main.restUrls')),
    re_path(r'^.*$', views.index),
]


