from datetime import datetime

from django.db import models
import uuid


# Create your models here.

class User(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    group = models.CharField(max_length=16)
    stud_number = models.CharField(max_length=16)
    role = models.CharField(max_length=10)
    phone_number = models.CharField(max_length=13)
    email = models.CharField(max_length=64)

    def __str__(self):
        return str(self.first_name) + ' ' + str(self.last_name) + ' ' + str(self.group)


class Classroom(models.Model):
    number = models.CharField(max_length=10, primary_key=True)
    current_user = models.ForeignKey(User, blank=True, null=True, related_name='classrooms', on_delete=models.CASCADE)

    def __str__(self):
        label = "Free" if self.current_user is None else "taken by " + str(self.current_user)
        return str(self.number) + ' ' + label


class Action(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=datetime.now())
    type = models.CharField(max_length=8)

    def __str__(self):
        return str(self.user.last_name) + ' ' + str(self.type) + ' ' + str(self.timestamp)
