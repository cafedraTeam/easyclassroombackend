# EasyClassroom

Backend for Easy Classroom project


**To start working:**

* *Clone this repo*

* *Create virtual environment and activate it* 

    * `virtualenv venv`
    
    * `source venv/bin/activate`
    
* *Install django* `pip install django=2.1.0`

* *Clone frontend repo:* `git clone https://svyatoslav00012@bitbucket.org/cafedraTeam/easyclassroombackend.git`

* *Read Readme on frontend repo to learn how to inject react in backend*

* *Launch server*

    * `py manage.py makemigrations`
    
    * `py manage.py migrate` 
    
    * `py manage.py runserver`
    
* *Done!*


**Frontend changes applys automatically**


####Happy coding!!!
